/*
  An example why smaller stubborn sets don't always produce the best reduction.
  After firing t1 or t2, there's two possible stubborn sets, with one and two transitions respectively.
  Choosing the singleton set yields 8 markings and 10 edges, whereas the larger set yields only 7 markings and 9 edges.

  source: (Figure 2)
  @article{valmari2017stubborn,
    title={Stubborn set intuition explained},
    author={Valmari, Antti and Hansen, Henri},
    journal={Transactions on Petri Nets and Other Models of Concurrency XII},
    pages={140--165},
    year={2017},
    publisher={Springer}
  }
*/

PLACE
p1, p2, p3, p4, p5, p6, p7, p8, p9;

MARKING p1 : 1, p7 : 1;

TRANSITION t1
  CONSUME p1 : 1, p7 : 1;
  PRODUCE p2 : 1, p8: 1;

TRANSITION t2
  CONSUME p1 : 1, p7 : 1;
  PRODUCE p3 : 1, p8 : 1;

TRANSITION t3
  CONSUME p2 : 1;
  PRODUCE p4 : 1;

TRANSITION t4
  CONSUME p2 : 1;
  PRODUCE p5 : 1;

TRANSITION t5
  CONSUME p3 : 1;
  PRODUCE p4 : 1;

TRANSITION t6
  CONSUME p3 : 1;
  PRODUCE p5 : 1;

TRANSITION t7
  CONSUME p4 : 1;
  PRODUCE p6 : 1;

TRANSITION t8
  CONSUME p5 : 1;
  PRODUCE p6 : 1;

TRANSITION t9
  CONSUME p8 : 1;
  PRODUCE p9 : 1;