/* Dmitry Zaitsev daze.ho.ua */
/* Modified for LoLA by Daniel Passauer */
/* Petri net model of squre grid with terminal devices (a single transition) on borders   */
/* k - grid size, p - number of packets in each buffer section, b - available buffer size */
/* output format - .lola file of LoLA*/

#include <stdio.h>
#include <stdlib.h>

#define IOBL 1

int main( int argc, char * argv[] )
{
 int k,i,j,b,p;

 if( argc < 4 )
 {
   printf("*** USAGE: sgen k p b > sg-k_p_b.lola\n");
   return(2);
 }
 else
 {
   k = atoi( argv[1] );
   p = atoi( argv[2] );
   b = atoi( argv[3] );
 }

/* PLACE */
printf("PLACE\n");
/* communication devices */
 for(i=1; i<=k; i++)
   for(j=1; j<=k; j++)
   {
     printf("pbl.%d.%d, ", i,j);

     printf("pb1.%d.%d, ", i,j);
     printf("pb2.%d.%d, ", i,j);
     printf("pb3.%d.%d, ", i,j);
     printf("pb4.%d.%d, ", i,j);

     printf("p1o.%d.%d, ", i,j);
     printf("p1ol.%d.%d, ", i,j);
     printf("p1i.%d.%d, ", i,j);
     printf("p1il.%d.%d, ", i,j);
     
     printf("p4i.%d.%d, ", i,j);
     printf("p4il.%d.%d, ", i,j);
     printf("p4o.%d.%d, ", i,j);
     printf("p4ol.%d.%d, ", i,j);

   }
   
 /* terminal devices */
 for(j=1; j<=k; j++)
 {
   printf("p1o.%d.%d, ", k+1,j);
   printf("p1ol.%d.%d, ", k+1,j);
   printf("p1i.%d.%d, ", k+1,j);
   printf("p1il.%d.%d, ", k+1,j);
 }
 for(i=1; i<=k; i++)
 {
   printf("p4i.%d.%d, ", i,k+1);
   printf("p4il.%d.%d, ", i,k+1);
   printf("p4o.%d.%d, ", i,k+1);
   printf("p4ol.%d.%d", i,k+1);
   if (i == k) {
    printf(";");
   } else {
    printf(", ");
   }
 }
 
 printf("\n");
 printf("\n");
   
/* MARKING */
printf("MARKING\n");
/* communication devices */
 for(i=1; i<=k; i++)
   for(j=1; j<=k; j++)
   {
     printf("pbl.%d.%d : %d, ", i,j,b );
     printf("pb1.%d.%d : %d, ", i,j,p );
     printf("pb2.%d.%d : %d, ", i,j,p );
     printf("pb3.%d.%d : %d, ", i,j,p );
     printf("pb4.%d.%d : %d, ", i,j,p );

     printf("p1ol.%d.%d : %d, ", i,j,IOBL );
     printf("p1il.%d.%d : %d, ", i,j,IOBL );
     
     printf("p4ol.%d.%d : %d, ", i,j,IOBL );
     printf("p4il.%d.%d : %d, ", i,j,IOBL );

   }
   
 /* terminal devices */
 for(j=1; j<=k; j++)
 {
   printf("p1ol.%d.%d : %d, ", k+1,j,IOBL );
   printf("p1il.%d.%d : %d, ", k+1,j,IOBL );
 }
 for(i=1; i<=k; i++)
 {
   printf("p4ol.%d.%d : %d, ", i,k+1,IOBL );
   printf("p4il.%d.%d : %d", i,k+1,IOBL );
   if (i == k) {
    printf(";");
   } else {
    printf(", ");
   }
 }
 
 printf("\n");
 printf("\n");
   
 /* TRANSITION */
 /* communication devices */
 for(i=1; i<=k; i++)
   for(j=1; j<=k; j++)
   {
     printf("TRANSITION t1o.%d.%d CONSUME p1ol.%d.%d : 1, pb1.%d.%d : 1; PRODUCE p1o.%d.%d : 1, pbl.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t1i2.%d.%d CONSUME p1i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1il.%d.%d : 1, pb2.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t1i3.%d.%d CONSUME p1i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1il.%d.%d : 1, pb3.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t1i4.%d.%d CONSUME p1i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1il.%d.%d : 1, pb4.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     
     printf("TRANSITION t4o.%d.%d CONSUME p4ol.%d.%d : 1, pb4.%d.%d : 1; PRODUCE p4o.%d.%d : 1, pbl.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t4i1.%d.%d CONSUME p4i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4il.%d.%d : 1, pb1.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t4i2.%d.%d CONSUME p4i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4il.%d.%d : 1, pb2.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );
     printf("TRANSITION t4i3.%d.%d CONSUME p4i.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4il.%d.%d : 1, pb3.%d.%d : 1;\n", i,j, i,j, i,j, i,j, i,j );

     printf("TRANSITION t3o.%d.%d CONSUME p1il.%d.%d : 1, pb3.%d.%d : 1; PRODUCE p1i.%d.%d : 1, pbl.%d.%d : 1;\n", i,j, i+1,j, i,j, i+1,j, i,j );
     printf("TRANSITION t3i1.%d.%d CONSUME p1o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1ol.%d.%d : 1, pb1.%d.%d : 1;\n", i,j, i+1,j, i,j, i+1,j, i,j );
     printf("TRANSITION t3i2.%d.%d CONSUME p1o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1ol.%d.%d : 1, pb2.%d.%d : 1;\n", i,j, i+1,j, i,j, i+1,j, i,j );
     printf("TRANSITION t3i4.%d.%d CONSUME p1o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p1ol.%d.%d : 1, pb4.%d.%d : 1;\n", i,j, i+1,j, i,j, i+1,j, i,j );

     printf("TRANSITION t2o.%d.%d CONSUME p4il.%d.%d : 1, pb2.%d.%d : 1; PRODUCE p4i.%d.%d : 1, pbl.%d.%d : 1;\n", i,j, i,j+1, i,j, i,j+1, i,j );
     printf("TRANSITION t2i1.%d.%d CONSUME p4o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4ol.%d.%d : 1, pb1.%d.%d : 1;\n", i,j, i,j+1, i,j, i,j+1, i,j );
     printf("TRANSITION t2i3.%d.%d CONSUME p4o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4ol.%d.%d : 1, pb3.%d.%d : 1;\n", i,j, i,j+1, i,j, i,j+1, i,j );
     printf("TRANSITION t2i4.%d.%d CONSUME p4o.%d.%d : 1, pbl.%d.%d : 1; PRODUCE p4ol.%d.%d : 1, pb4.%d.%d : 1;\n", i,j, i,j+1, i,j, i,j+1, i,j );
   }

 /* terminal devices */
 for(j=1; j<=k; j++)
 {
   /* upper */
   printf("TRANSITION tf-u.%d CONSUME p1o.%d.%d : 1, p1il.%d.%d : 1; PRODUCE p1ol.%d.%d : 1, p1i.%d.%d : 1;\n", j, 1,j, 1,j, 1,j, 1,j );
   /* down */
   printf("TRANSITION tf-d.%d CONSUME p1i.%d.%d : 1, p1ol.%d.%d : 1; PRODUCE p1il.%d.%d : 1, p1o.%d.%d : 1;\n", j, k+1,j, k+1,j, k+1,j, k+1,j );
 }
 for(i=1; i<=k; i++)
 {
   /* left */
   printf("TRANSITION tf-l.%d CONSUME p4o.%d.%d : 1, p4il.%d.%d : 1; PRODUCE p4ol.%d.%d : 1, p4i.%d.%d : 1;\n", i, i,1, i,1, i,1, i,1 );
   /* right */
   printf("TRANSITION tf-r.%d CONSUME p4i.%d.%d : 1, p4ol.%d.%d : 1; PRODUCE p4il.%d.%d : 1, p4o.%d.%d : 1;\n", i, i,k+1, i,k+1, i,k+1, i,k+1 );
 }
 return 0;
} /* main */

