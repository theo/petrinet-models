# Petrinet models

## Document models!
If your model was designed to serve a particular purpose, please write a short comment (`/* ... */`) at the top of the file and/or into some markdown file close to the model.

If you found the model somewhere, please add the source, possibly even in BibTex format, to simplify referencing it in the future.

## Draw nets with Mermaid

You can draw Petri nets with [Mermaid](https://mermaid.js.org/), using `place_id((place_label))` to draw places.

```mermaid
graph
  p1((p1));p2((p2));p3((p3));p4((p4));p5((p5));
  p1 --> t1;t1 --> p2;
  p2 --> t2;t2 --> p3;
  p5 --> t3;t3 --> p4;
  p5 --> t4;t4 --> p5;
  p1 --> t5;p4 --> t5;t5 --> p1;t5 --> p5;
```

Petripy/Slowla can generate such an output using `slowla draw --format=mermaid net.lola` (currently only for non-modular nets).
