/*
    From https://mcc.lip6.fr/2024/pdf/TriangularGrid-form.pdf for k=2, p=1, b=1

    Modular Version of above net that splits off 2 modules: upper layer, bottom layer; by declaring to3^2.2, to3^1.1 as interface transitions

    Synchronisation graph stats:
        Vertices: 22
        Edges:    40
    
    Local Reachability graph stats:
        Upper module:
            Vertices:      1.118
            Segments:         21
        Bottom module:
            Vertices: 44.971.853
            Segments:         20

*/

/* Upper layer */
MODULE Upper
NET
PLACE
pi1^1.1, pil1^1.1, po1^1.1, pol1^1.1, 
pi2^1.1, pil2^1.1, po2^1.1, pol2^1.1, 
pi3^1.1, pil3^1.1,
pbl^1.1, pb1^1.1, pb2^1.1, pb3^1.1;

MARKING
pil1^1.1 : 1, pol1^1.1 : 1, 
pil2^1.1 : 1, pol2^1.1 : 1, 
pil3^1.1 : 1,
pbl^1.1 : 1, pb1^1.1 : 1, pb2^1.1 : 1, pb3^1.1 : 1;

TRANSITION t^1.3 CONSUME pil1^1.1 : 1, po1^1.1 : 1; PRODUCE pi1^1.1 : 1, pol1^1.1 : 1;

TRANSITION ti1-2^1.1 CONSUME pbl^1.1 : 1, pi1^1.1 : 1; PRODUCE pil1^1.1 : 1, pb2^1.1 : 1;
TRANSITION ti1-3^1.1 CONSUME pbl^1.1 : 1, pi1^1.1 : 1; PRODUCE pil1^1.1 : 1, pb3^1.1 : 1;
TRANSITION ti2-1^1.1 CONSUME pbl^1.1 : 1, pi2^1.1 : 1; PRODUCE pil2^1.1 : 1, pb1^1.1 : 1;
TRANSITION ti2-3^1.1 CONSUME pbl^1.1 : 1, pi2^1.1 : 1; PRODUCE pil2^1.1 : 1, pb3^1.1 : 1;
TRANSITION ti3-1^1.1 CONSUME pbl^1.1 : 1, pi3^1.1 : 1; PRODUCE pil3^1.1 : 1, pb1^1.1 : 1;
TRANSITION ti3-2^1.1 CONSUME pbl^1.1 : 1, pi3^1.1 : 1; PRODUCE pil3^1.1 : 1, pb2^1.1 : 1;
TRANSITION to1^1.1 CONSUME pol1^1.1 : 1, pb1^1.1 : 1; PRODUCE po1^1.1 : 1, pbl^1.1 : 1;
TRANSITION to2^1.1 CONSUME pol2^1.1 : 1, pb2^1.1 : 1; PRODUCE po2^1.1 : 1, pbl^1.1 : 1;

TRANSITION t^1.4 CONSUME pil2^1.1 : 1, po2^1.1 : 1; PRODUCE pi2^1.1 : 1, pol2^1.1 : 1;
/* Interfaces */
TRANSITION to3^1.1 CONSUME pb3^1.1 : 1;  PRODUCE pbl^1.1 : 1;
TRANSITION to3^2.2 CONSUME pil3^1.1 : 1; PRODUCE pi3^1.1 : 1;

/* Bottom layer */
MODULE Bottom
NET
PLACE
/* From upper layer */
po3^1.1, pol3^1.1,
/* Bottom layer 2.1 */
pi1^2.1, pil1^2.1, po1^2.1, pol1^2.1, 
pi2^2.1, pil2^2.1, po2^2.1, pol2^2.1, 
pi3^2.1, pil3^2.1, po3^2.1, pol3^2.1,
pbl^2.1, pb1^2.1, pb2^2.1, pb3^2.1,
/* Bottom layer 2.2 */
pbl^2.2, pb1^2.2, pb2^2.2, pb3^2.2,
/* Bottom layer 2.3 */
pi1^2.3, pil1^2.3, po1^2.3, pol1^2.3, 
pi2^2.3, pil2^2.3, po2^2.3, pol2^2.3, 
pi3^2.3, pil3^2.3, po3^2.3, pol3^2.3,
pbl^2.3, pb1^2.3, pb2^2.3, pb3^2.3;

MARKING
/* From upper layer */
pol3^1.1 : 1,
/* Bottom layer 2.1 */
pil1^2.1 : 1, pol1^2.1 : 1, 
pil2^2.1 : 1, pol2^2.1 : 1, 
pil3^2.1 : 1, pol3^2.1 : 1,
pbl^2.1 : 1, pb1^2.1 : 1, pb2^2.1 : 1, pb3^2.1 : 1,
/* Bottom layer 2.2 */
pbl^2.2 : 1, pb1^2.2 : 1, pb2^2.2 : 1, pb3^2.2 : 1,
/* Bottom layer 2.3 */
pil1^2.3 : 1, pol1^2.3 : 1, 
pil2^2.3 : 1, pol2^2.3 : 1, 
pil3^2.3 : 1, pol3^2.3 : 1,
pbl^2.3 : 1, pb1^2.3 : 1, pb2^2.3 : 1, pb3^2.3 : 1;

/* Bottom layer 2.1 */
TRANSITION ti1-2^2.1 CONSUME pbl^2.1 : 1, pi1^2.1 : 1; PRODUCE pil1^2.1 : 1, pb2^2.1 : 1;
TRANSITION ti1-3^2.1 CONSUME pbl^2.1 : 1, pi1^2.1 : 1; PRODUCE pil1^2.1 : 1, pb3^2.1 : 1;
TRANSITION ti2-1^2.1 CONSUME pbl^2.1 : 1, pi2^2.1 : 1; PRODUCE pil2^2.1 : 1, pb1^2.1 : 1;
TRANSITION ti2-3^2.1 CONSUME pbl^2.1 : 1, pi2^2.1 : 1; PRODUCE pil2^2.1 : 1, pb3^2.1 : 1;
TRANSITION ti3-1^2.1 CONSUME pbl^2.1 : 1, pi3^2.1 : 1; PRODUCE pil3^2.1 : 1, pb1^2.1 : 1;
TRANSITION ti3-2^2.1 CONSUME pbl^2.1 : 1, pi3^2.1 : 1; PRODUCE pil3^2.1 : 1, pb2^2.1 : 1;
TRANSITION to1^2.1 CONSUME pol1^2.1 : 1, pb1^2.1 : 1; PRODUCE po1^2.1 : 1, pbl^2.1 : 1;
TRANSITION to2^2.1 CONSUME pol2^2.1 : 1, pb2^2.1 : 1; PRODUCE po2^2.1 : 1, pbl^2.1 : 1;
TRANSITION to3^2.1 CONSUME pol3^2.1 : 1, pb3^2.1 : 1; PRODUCE po3^2.1 : 1, pbl^2.1 : 1;

/* Bottom layer 2.2 */
TRANSITION ti1-2^2.2 CONSUME pbl^2.2 : 1, po1^2.3 : 1; PRODUCE pol1^2.3 : 1, pb2^2.2 : 1;
TRANSITION ti1-3^2.2 CONSUME pbl^2.2 : 1, po1^2.3 : 1; PRODUCE pol1^2.3 : 1, pb3^2.2 : 1;
TRANSITION ti2-1^2.2 CONSUME pbl^2.2 : 1, po2^2.1 : 1; PRODUCE pol2^2.1 : 1, pb1^2.2 : 1;
TRANSITION ti2-3^2.2 CONSUME pbl^2.2 : 1, po2^2.1 : 1; PRODUCE pol2^2.1 : 1, pb3^2.2 : 1;
TRANSITION ti3-1^2.2 CONSUME pbl^2.2 : 1, po3^1.1 : 1; PRODUCE pol3^1.1 : 1, pb1^2.2 : 1;
TRANSITION ti3-2^2.2 CONSUME pbl^2.2 : 1, po3^1.1 : 1; PRODUCE pol3^1.1 : 1, pb2^2.2 : 1;
TRANSITION to1^2.2 CONSUME pil1^2.3 : 1, pb1^2.2 : 1; PRODUCE pi1^2.3 : 1, pbl^2.2 : 1;
TRANSITION to2^2.2 CONSUME pil2^2.1 : 1, pb2^2.2 : 1; PRODUCE pi2^2.1 : 1, pbl^2.2 : 1;

/* Bottom layer 2.3 */
TRANSITION ti1-2^2.3 CONSUME pbl^2.3 : 1, pi1^2.3 : 1; PRODUCE pil1^2.3 : 1, pb2^2.3 : 1;
TRANSITION ti1-3^2.3 CONSUME pbl^2.3 : 1, pi1^2.3 : 1; PRODUCE pil1^2.3 : 1, pb3^2.3 : 1;
TRANSITION ti2-1^2.3 CONSUME pbl^2.3 : 1, pi2^2.3 : 1; PRODUCE pil2^2.3 : 1, pb1^2.3 : 1;
TRANSITION ti2-3^2.3 CONSUME pbl^2.3 : 1, pi2^2.3 : 1; PRODUCE pil2^2.3 : 1, pb3^2.3 : 1;
TRANSITION ti3-1^2.3 CONSUME pbl^2.3 : 1, pi3^2.3 : 1; PRODUCE pil3^2.3 : 1, pb1^2.3 : 1;
TRANSITION ti3-2^2.3 CONSUME pbl^2.3 : 1, pi3^2.3 : 1; PRODUCE pil3^2.3 : 1, pb2^2.3 : 1;
TRANSITION to1^2.3 CONSUME pol1^2.3 : 1, pb1^2.3 : 1; PRODUCE po1^2.3 : 1, pbl^2.3 : 1;
TRANSITION to2^2.3 CONSUME pol2^2.3 : 1, pb2^2.3 : 1; PRODUCE po2^2.3 : 1, pbl^2.3 : 1;
TRANSITION to3^2.3 CONSUME pol3^2.3 : 1, pb3^2.3 : 1; PRODUCE po3^2.3 : 1, pbl^2.3 : 1;

/* Interfaces */
TRANSITION to3^1.1 CONSUME pol3^1.1 : 1; PRODUCE po3^1.1 : 1;
TRANSITION to3^2.2 CONSUME pb3^2.2 : 1;  PRODUCE pbl^2.2 : 1;

/* Ends*/
TRANSITION t^3.1 CONSUME pil3^2.1 : 1, po3^2.1 : 1; PRODUCE pi3^2.1 : 1, pol3^2.1 : 1;
TRANSITION t^3.3 CONSUME pil3^2.3 : 1, po3^2.3 : 1; PRODUCE pi3^2.3 : 1, pol3^2.3 : 1;
TRANSITION t^2.3 CONSUME pil1^2.1 : 1, po1^2.1 : 1; PRODUCE pi1^2.1 : 1, pol1^2.1 : 1;
TRANSITION t^2.4 CONSUME pil2^2.3 : 1, po2^2.3 : 1; PRODUCE pi2^2.3 : 1, pol2^2.3 : 1;

/* Fuse Upper and Lower Module to3^1.1 and to3^2.2 */
FUSIONSET to3^1.1 Upper::to3^1.1, Bottom::to3^1.1;
FUSIONSET to3^2.2 Upper::to3^2.2, Bottom::to3^2.2;