#!/bin/bash

# Generates modular philosophes with upper - lower modules with
# lower , lower + 1 , ... , upper - 1 , upper many philosophes per module respectively
# Module with lower modules will take forks in reverse to smash symmetries

# Example: lower=1, upper=3
# Result: 3 Modules with 1, 2, 3 philosophes respectively
# Module with 1 philosoph takes forks in revers


lower=$1
upper=$2

# Modules, generates smallest module in reverse
for ((module=upper;module>lower;module--))
do
    printf "MODULE %s\n" "$module"
    printf "NET\n"

    # PLACE
    printf "PLACE\n"
    places=(ea hl th fo)
    for place in "${places[@]}"
    do
        printf "\t"
        for ((i=1;i<=module;i++))
        do
            printf "%s.%s" "$place" "$i"
            if [ "$place" != "fo" ] || ((i < module))
            then
                printf ", "
            else 
                printf ";"
            fi
        done
        printf "\n"
    done
    printf "\n"

    # MARKING
    printf "MARKING\n"
    places=(th fo)
    for place in "${places[@]}"
    do
        printf "\t"
        for ((i=1;i<=module;i++))
        do
            printf "%s.%s : 1" "$place" "$i"
            if [ "$place" != "fo" ] || ((i < module))
            then
                printf ", "
            else 
                printf ";"
            fi
        done
        printf "\n"
    done
    printf "\n"

    # TRANSITION
    # tl.#
    for ((i=1;i<=module;i++))
    do
        printf "TRANSITION tl.%s\n" "$i"
        printf "\tCONSUME\n"
        printf "\t\tth.%s : 1,\n" "$i"
        printf "\t\tfo.%s : 1;\n" "$i"
        printf "\tPRODUCE\n"
        printf "\t\thl.%s : 1;\n" "$i"
    done
    printf "\n"
    # tr.#
    for ((i=1;i<=module;i++))
    do
        printf "TRANSITION tr.%s\n" "$i"
        printf "\tCONSUME\n"
        if ((i == module))
        then
            printf "\t\thl.%s : 1;\n" "$i"
        else
            printf "\t\thl.%s : 1,\n" "$i"
            printf "\t\tfo.%s : 1;\n" "$((i + 1))"
        fi
        printf "\tPRODUCE\n"
        printf "\t\thl.%s : 1;\n" "$i"
    done
    printf "\n"
    # r.#
    for ((i=1;i<=module;i++))
    do
        printf "TRANSITION r.%s\n" "$i"
        printf "\tCONSUME\n"
        printf "\t\tea.%s : 1;\n" "$i"
        printf "\tPRODUCE\n"
        printf "\t\tfo.%s : 1,\n" "$i"
        if ((i != module))
        then
            printf "\t\tfo.%s : 1,\n" "$((i + 1))"
        fi
        printf "\t\tth.%s : 1;\n" "$i"
    done
    printf "\n"
    # nt
    printf "TRANSITION nt\n"
    printf "\tCONSUME\n"
    printf "\t\tfo.1 : 1;\n"
    printf "\tPRODUCE;\n"
    printf "\n"
    # nr
    printf "TRANSITION nr\n"
    printf "\tCONSUME;\n"
    printf "\tPRODUCE\n"
    printf "\t\tfo.1 : 1;\n"
    printf "\n"

    printf "\n"
done

# reverse module 

module="$lower"
printf "MODULE %s\n" "$module"
printf "NET\n"

# PLACE
printf "PLACE\n"
places=(ea hr th fo)
for place in "${places[@]}"
do
    printf "\t"
    for ((i=1;i<=module;i++))
    do
        printf "%s.%s" "$place" "$i"
        if [ "$place" != "fo" ] || ((i < module))
        then
            printf ", "
        else 
            printf ";"
        fi
    done
    printf "\n"
done
printf "\n"

# MARKING
printf "MARKING\n"
places=(th fo)
for place in "${places[@]}"
do
    printf "\t"
    for ((i=1;i<=module;i++))
    do
        printf "%s.%s : 1" "$place" "$i"
        if [ "$place" != "fo" ] || ((i < module))
        then
            printf ", "
        else 
            printf ";"
        fi
    done
    printf "\n"
done
printf "\n"

# TRANSITION
# tr.#
for ((i=1;i<=module;i++))
do
    printf "TRANSITION tr.%s\n" "$i"
    printf "\tCONSUME\n"
    if ((i == module))
    then
        printf "\t\tth.%s : 1;\n" "$i"
    else
        printf "\t\tth.%s : 1,\n" "$i"
        printf "\t\tfo.%s : 1;\n" "$((i + 1))"
    fi
    printf "\tPRODUCE\n"
    printf "\t\thr.%s : 1;\n" "$i"
done
printf "\n"
# tl.#
for ((i=1;i<=module;i++))
do
    printf "TRANSITION tl.%s\n" "$i"
    printf "\tCONSUME\n"
    printf "\t\thr.%s : 1,\n" "$i"
    printf "\t\tfo.%s : 1;\n" "$i"
    printf "\tPRODUCE\n"
    printf "\t\tea.%s : 1;\n" "$i"
done
printf "\n"
# r.#
for ((i=1;i<=module;i++))
do
    printf "TRANSITION r.%s\n" "$i"
    printf "\tCONSUME\n"
    printf "\t\tea.%s : 1;\n" "$i"
    printf "\tPRODUCE\n"
    printf "\t\tfo.%s : 1,\n" "$i"
    if ((i != module))
    then
        printf "\t\tfo.%s : 1,\n" "$((i + 1))"
    fi
    printf "\t\tth.%s : 1;\n" "$i"
done
printf "\n"
# nt
printf "TRANSITION nt\n"
printf "\tCONSUME\n"
printf "\t\tfo.1 : 1;\n"
printf "\tPRODUCE;\n"
printf "\n"
# nr
printf "TRANSITION nr\n"
printf "\tCONSUME;\n"
printf "\tPRODUCE\n"
printf "\t\tfo.1 : 1;\n"
printf "\n"

printf "\n"

# Fusion sets
for ((first=lower;first<upper;first++))
do
    printf "FUSIONSET %s_takes_from_%s\n" "$first" "$((first + 1))"
    printf "\t%s::tr.%s, %s::nt;\n" "$first" "$first" "$((first + 1))"
    printf "FUSIONSET %s_releases_for_%s\n" "$first" "$((first + 1))"
    printf "\t%s::r.%s, %s::nr;\n" "$first" "$first" "$((first + 1))"
done
# Closing fusion set
printf "FUSIONSET %s_takes_from_%s\n" "$upper" "$lower"
printf "\t%s::tr.%s, %s::nt;\n" "$upper" "$upper" "$lower"
printf "FUSIONSET %s_releases_for_%s\n" "$upper" "$lower"
printf "\t%s::r.%s, %s::nr;\n" "$upper" "$upper" "$lower"